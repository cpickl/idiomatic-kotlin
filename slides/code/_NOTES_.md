# NOTES

* show everything in live coding
* prepare slides for later on (use as cheatsheet)
* at beginning, tell people use feedback:
  * "heart" indicating "that's useful, please continue"
  * "thumb up" indicating i know that already
    * possibility of veto via "heart", otherwise skip

# TODO

* [x] go through: kotlin11slides
* [x] go through: kotlin101slides
* [x] go through: awesomekotlin
* [x] go through: kotlinlang.org website
* [ ] go through: PMS/other services (legacy) code, and handpick what's needed to be mentioned 
* [ ] add some more programme nerd meme funny pics
* [x] bring content in reasonable order & complete ToC
* [ ] for each topic, incorporate example application in idiomatic-kotlin project code

# CONTENT

* ~~SDK highlights~~:
  * ~~collections~~
  * ~~takeIf/Unless~~
  * ~~onEach~~
  * ~~grouping/associatedBy~~ (functionals later)
* ~~language features~~:
  * ~~infix notation~~
  * ~~destructor~~
  * ~~explicit property setters/getters~~
  * ~~underscore~~ (unused numbers, destruction values, and type arguments)
  * ~~operator overloading~~ (https://kotlinlang.org/docs/operator-overloading.html)
  * ~~range expressions~~
* ~~language features advanced~~:
  * ~~tail recursion~~
  * ~~sealed classes~~ (algebraic datastructures FTW! tree structure)
  * ~~when~~ (switch but more like "soft" pattern matching)
  * ~~inline~~ (properties/functions/classes)
  * ~~Nothing, Any~~
  * ~~@Deprecated~~ (warn/error; and replace with IntelliJ)
  * ~~objects~~ (some magic; static?! companion objects)
* ~~generics~~: (https://kotlinlang.org/docs/generics.html)
  * ~~in/out~~
  * ~~boundaries~~ (where)
  * ~~reified & inline~~ (remember type erasure; thanks java, why not like C#?)
* ~~functionals~~:
  * ~~fun interface~~ (vs typealias for function signatures)
  * ~~basic functional paradigm~~
  * ~~definition~~ (higher order funcs = funs as in/out, purity)
  * ~~crossinline, noinline~~ (reason explained)
  * ~~SDK functionals~~ (also, apply, let, run, with; map, filter, fold, ...)
  * ~~bound callable references~~ (instance::method)
* ~~extensions~~
  * ~~functions, properties, member extensions~~ (within a class-context)
  * ~~function types with receiver~~
  * ~~nullable receiver~~
  * ~~companion object extensions~~
* ~~delegation~~: https://kotlinlang.org/docs/delegation.html
  * ~~lazy~~
  * ~~class delegates~~ (interface by)
  * ~~delegated properties~~ (operators: getValue, setValue, provideDelegate)
* ~~tips'n'tricks~~
  * ~~pseudo ctor~~ (private ctor, public static invoke operator => DDD & Either)
  * ~~Enum Finder~~ (Api/SqlEnum type with reusable "lookup mechanism")
* ~~own DSL~~ for typesafe builders (HTML example; Scope control: @DslMarker)
* https://github.com/Kotlin/KEEP

# Not content

* coroutines
* reflection
* contracts (in tests especially great -> assert)
* context receivers
* FP with arrow || funKTionale
* MPP