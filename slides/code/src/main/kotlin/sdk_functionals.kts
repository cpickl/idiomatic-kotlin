fun <T> T.also(f: (T) -> Unit): T {
    f(this)
    return this
}
fun <T> T.apply(f: T.() -> Unit): T {
    this.f()
    return this
}
fun <T, R> T.let(f: (T) -> R): R {
    return f(this)
}
fun <T, R> with(x: T, f: T.() -> R): R {
    return x.f()
}