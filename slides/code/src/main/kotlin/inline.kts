// functions
inline fun optimized(code: () -> Unit) {}

// classes
@JvmInline
value class Foo(val wrapped: Int)

// properties
class Bar {
    inline val x: Int get() = 42
}