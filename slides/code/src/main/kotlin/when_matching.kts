val foo = 42
when {
    foo in 1..5 -> println("strange")
    foo <= 10 -> println("lil")
    foo <= 20 -> println("med")
    else -> println("big")
}