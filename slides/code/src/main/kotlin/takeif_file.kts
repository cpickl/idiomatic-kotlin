import java.io.File

val f1 = File("path")
if (!f1.exists()) error("ouch")

// takeIf works well with elvis operator
val f2 = File("path").takeIf(File::exists)
    ?: error("ouch")