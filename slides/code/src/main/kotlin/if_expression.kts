val x = 21
val y = 42

// `if` is an expression, not a statement:
val z = if(x == y) 1337 else 0

// same for `when`:
val z2 = when(x) {
    12 -> "small"
    21 -> "big"
    else -> "nope"
}