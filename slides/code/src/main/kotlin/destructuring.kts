val map = mapOf(1 to "one", 2 to "two")
println(map.mapValues {
    // destructure entries on the fly
    (key, value) -> "$key -> $value!" })

data class Person(
    val name: String, val age: Int)
val p = Person("foo", 42)
// data classes provide their components
p.let { ( n, a) -> println("$n is $a") }