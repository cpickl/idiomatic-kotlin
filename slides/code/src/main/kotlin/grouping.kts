val list = listOf("anna", "otto", "oscar")

list
    .groupBy(String::first)
    .mapValues { (_, list) -> list.size } // a=1, o=2
// creates intermediate map|\pause|

list
    .groupingBy(String::first)
    .eachCount() // invokes foldTo()