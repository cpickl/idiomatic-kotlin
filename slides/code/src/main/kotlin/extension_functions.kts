interface Foo

fun Foo.bar() {}

val f: Foo = TODO()
f.bar()
// static magic; not OOP; requires import.

fun String.ext() = "no more StringUtil $this"