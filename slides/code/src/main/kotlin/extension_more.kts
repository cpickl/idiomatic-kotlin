fun String?.lengthOrZero(): Int =
    this?.length ?: 0

val String?.lengthOrZero: Int get() =
    this?.length ?: 0

class Foo {
    private fun String.bar() {
        this@Foo
        this
    }
    fun f(input: String) {
        input.bar()
    }
}