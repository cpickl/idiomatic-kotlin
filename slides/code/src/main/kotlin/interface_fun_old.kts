interface Foo {
  fun f(x: Int)
}

fun Foo(code: (Int) -> Unit) = object: Foo {
  override fun f(x: Int) {
    code(x)
  }
}

val foo = Foo {
  println("Got: $it")
}
foo.f(42)