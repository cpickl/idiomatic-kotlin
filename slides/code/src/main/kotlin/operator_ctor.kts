class Foo private constructor(val age: Int) {
  companion object {
    operator fun invoke(age: Int): Foo? =
      if (age < 0) null
      else Foo(age)
  }
}

val x: Foo? = Foo(-1)
val y: Foo? = Foo(13)