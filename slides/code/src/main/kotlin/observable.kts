import kotlin.properties.Delegates

class Foo {
  var x: String by Delegates.observable("") {
    prop, x, y -> println("$prop: $x -> $y")
  }
}

Foo().x = "haha"