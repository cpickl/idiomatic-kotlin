// Any == java.lang.Object
// Nothing == java.lang.Void

// premature bail out:
inline fun myTODO(): Nothing =
    throw Exception()

fun foo(): Any {
    myTODO()
    TODO()
    // no return needed
}

// or as type parameter: List<Nothing>