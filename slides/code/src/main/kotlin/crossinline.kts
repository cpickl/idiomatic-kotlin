fun foo() {
  println("A")
  bar {
    println("B")
    // non-local return forbidden
    // only: return@bar
  }
  println("C")
}
inline fun bar(crossinline f: () -> Unit) {
  f()
}