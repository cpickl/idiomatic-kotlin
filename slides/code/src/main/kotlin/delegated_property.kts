import kotlin.reflect.KProperty
class Foo { var d: String by Delegate() }
class Delegate {
  operator fun getValue(
    thiz: Any?, p: KProperty<*>) =
    "$thiz.${p.name} read"
  operator fun setValue(
    thiz: Any?, p: KProperty<*>, v: String) {
    println("$thiz.${p.name} = $v")
  }
}
val f = Foo()
println(f.d)
f.d = "set"