class Foo {
  // must be private or local
  private fun f() = object {
    val bar: String = "xxx"
  }

  fun print() {
    println(f().bar)
  }
}