class Foo {
    companion object {
        fun pseduoStatic() {}
    }
}
Foo.pseduoStatic()

// in src/test/kotlin
fun Foo.Companion.testInstance() = Foo()
Foo.testInstance()