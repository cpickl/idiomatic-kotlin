enum class RGB { RED, GREEN, BLUE }

val rgbs = enumValues<RGB>()
val red = enumValueOf<RGB>("RED")

println(enumValues<RGB>()
    .joinToString(transform = RGB::name))
// RED, GREEN, BLUE