fun <T> nope() {
  // println(T::class.java.name)
}
fun <T> ugly(type: Class<T>) {
    println(type.name)
}
ugly<String>(String::class.java)

inline fun <reified T> foo(): T {
  println(T::class.java.name)
  TODO("implement me")
}
foo<String>()
val x: String = foo()