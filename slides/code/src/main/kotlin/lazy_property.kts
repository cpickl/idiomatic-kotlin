val foo by lazy {
    println("computing...")
    42
}

// is computing.
println("life meaning #1: $foo")
// is not computing.
println("life meaning #2: $foo")