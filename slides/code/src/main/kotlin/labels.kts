outer@ for (i in 1..100) {
  for (j in 1..100) {
    // otherwise stop inner loop
    if (j == 42) break@outer
  }
}

fun f(): Int {
  (1..10).forEach foo@{
    if(it == 1) {
      return@foo
    }
  }
  return 42
}