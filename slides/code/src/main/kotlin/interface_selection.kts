interface A {
  fun foo() {}
}
interface B {
  fun foo() {}
}

class X : A, B {
  override fun foo() {
    super<A>.foo()
    super<B>.foo()
  }
}