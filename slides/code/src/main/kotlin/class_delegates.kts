interface Foo {
    fun f()
}

class Bar(private val foo: Foo) : Foo {
  // manually delegate
  override fun f() {
    foo.f()
  }
}

class Baz(private val foo: Foo) : Foo by foo