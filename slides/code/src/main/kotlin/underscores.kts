// well known
val decimal = 1_000_000
val hexaDecimal= 0x11_FF_22_BB
val bytes = 0b00000000_11111111

// might be lesser known
mapOf(1 to "one").map { (k, _) -> k }

// least known
interface Bar<S>
fun <B: Bar<S>, S> foo() {}
foo<Bar<String>, _>()