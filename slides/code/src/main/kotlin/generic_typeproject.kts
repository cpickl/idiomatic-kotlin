fun copy(from: Array<out Any>,
         to: Array<Any>) {
  // Array<? extends Object>
}

fun fill(dest: Array<in String>,
         value: String) {
  // Array<? super String>
}