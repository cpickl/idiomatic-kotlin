fun interface Foo {
  fun f(bar: Int)
}

// to cumbersome/wrong approach
val foo1 = object: Foo {
    override fun f(bar: Int) { }
}

// using a lambda
val foo2 = Foo { }

fun doWithFoo(foo: Foo) { }
doWithFoo { }