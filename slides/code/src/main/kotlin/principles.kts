// functionals == higher order functions
fun takeFunction(function: () -> Unit) {}
fun giveFunction(): () -> Unit { TODO() }

fun logic(x: Int, computer: (Int) -> Int) {
  val result = computer(x)
}

// functions as 1st class citizens
val f: (Int) -> Int = { x: Int -> x * 42 }
logic(21, f)