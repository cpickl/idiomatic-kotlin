interface Foo<out T> { // covariant
  fun produce(): T
  // fun consume(t: T)
}
val strings: Foo<String> = TODO()
val anys: Foo<Any> = strings

interface Bar<in T> { // contravariant
  // fun produce(): T
  fun consume(t: T)
}