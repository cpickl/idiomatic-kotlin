sealed interface Node {
  class Leaf(val value: Any) : Node
  class Branch(val left: Node, val right: Node) : Node
}
fun foo(root: Node) {
  when(root) {
    is Node.Branch -> foo(root.left)
    is Node.Leaf -> root.value
  }
}