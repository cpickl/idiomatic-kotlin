typealias Nicer = (String) -> String
typealias Ruder = (String) -> String

fun foo(nice: Nicer) {
    println(nice("Foo"))
}

val rude: Ruder = { "$it!!!"}
foo(nice = rude) // oops