import kotlin.reflect.KFunction1
import kotlin.reflect.KFunction2

class Foo {
  fun bar() { }
  fun bar2(param: Int) { }
}

val foo = Foo()
val barRef: KFunction1<Foo, Unit> = Foo::bar
val bar2Ref: KFunction2<Foo, Int, Unit> =
  Foo::bar2

barRef(foo)
bar2Ref(foo, 42)