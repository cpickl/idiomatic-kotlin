fun <T : Comparable<T>> foo(ts: List<T>) {
  ts.sorted()
}

fun <T : Runnable> bar(ts: List<T>) {
  ts.forEach(Runnable::run)
}

fun <T> foobar(ts: List<T>)
    where T : Comparable<T>, T : Runnable {
  ts.sorted().forEach(Runnable::run)
}
