val numbers = "\\d+".toRegex()
val strings = listOf("a", "1")

// oldschool approach with lambdas:
strings.filter { numbers.matches(it) }
       .forEach { println(it) }

// or using method references:
strings.filter(numbers::matches)
       .forEach(::println)