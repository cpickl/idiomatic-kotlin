sealed interface Error {
  val message: String
  object DatabaseError : Error {
    override val message = "DB crap"
  }
  class GenericError(
    override val message: String) : Error
}
fun foo(error: Error) {
  when(error) {
    Error.DatabaseError -> println("DB!")
    is Error.GenericError -> println("Or?")
  }
}