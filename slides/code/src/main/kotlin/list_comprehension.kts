IntArray(4) { it * 2 }.toList()
// [0, 2, 4, 6]

List(4) { it * 2 }
MutableList(4) { it * 2 }

// still not as powerfull as in haskell