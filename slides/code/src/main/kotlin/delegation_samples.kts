import kotlin.reflect.KProperty

class LazySample {
    // read only (not var), as setValue is not defined in CustomLazy
    val lazilyEvaluated by lazy2 { 40 + 2 }

    val sdkOwnLazy by lazy { 20 + 1 }
}

fun <T> lazy2(initializer: () -> T) = CustomLazy(initializer)

class CustomLazy<T>(
    private val initializer: () -> T
) {

    private var value: T? = null

    operator fun getValue(thiz: Any?, property: KProperty<*>): T =
        if (value == null) {
            println("GET: ${thiz!!.javaClass.simpleName}.${property.name} - computing lazy value")
            val calculated = initializer()
            value = calculated
            calculated
        } else {
            println("GET: ${thiz!!.javaClass.simpleName}.${property.name} - reading cached value")
            value!!
        }
}

println("LAZY ...")
val lazy = LazySample()
println("get #1")
lazy.lazilyEvaluated
println("get #2")
lazy.lazilyEvaluated
println("get #3")
lazy.lazilyEvaluated
println()
println("==============")
println()

// =====================================================================================================================

class ObserveSample {
    var observed: Int? by Observable()
}

class Observable<T> {

    private var value: T? = null

    operator fun getValue(thiz: Any?, property: KProperty<*>): T? {
        println("GET: ${thiz!!.javaClass.simpleName}.${property.name} => $value")
        return value
    }

    operator fun setValue(thiz: Any?, property: KProperty<*>, value: T?) {
        println("SET: ${thiz!!.javaClass.simpleName}.${property.name} = old[${this.value}] => new[$value]")
        this.value = value
    }
}

println("OBSERVE ...")
val observable = ObserveSample()
println("get #1")
observable.observed
println("set")
observable.observed = 42
println("get #2")
observable.observed