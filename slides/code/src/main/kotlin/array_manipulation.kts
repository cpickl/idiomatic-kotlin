val a1 = arrayOf("a", "b")
a1.toString()
// [Ljava.lang.String;\@1b3af
a1.contentToString()
// [a, b]

val a2 = arrayOf(arrayOf("a"), arrayOf("b"))
a2.contentToString()
// [[Ljava.lang.String;@6b884d57, [L...
a2.contentDeepToString()
// [[a], [b]]