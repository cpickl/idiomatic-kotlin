import java.math.BigInteger

// prefix notation
fun foo(x: Int, y: Int) = x + y
foo(1, 2)

infix fun Int.bar(y: Int) = this + y
1.bar(2)
1 bar 2

infix fun BigInteger.`+`(x: BigInteger) =
  this.add(x)
BigInteger.ONE `+` BigInteger.TEN