fun f(provider: () -> String) {
    val value by lazy(provider)

    // short circuit evaluation FTW
    if (true && value.isNotEmpty()) {
        // computed result will be cached
        println(value)
    }
}