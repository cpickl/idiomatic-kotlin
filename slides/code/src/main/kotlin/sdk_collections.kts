emptyMap<String, Int>()
mapOf("a" to 1)
mutableMapOf("b" to 2)

emptyList<Int>()
listOf(1, 2)
mutableListOf(1, 2)

// set, array, sequence