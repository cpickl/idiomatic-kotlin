import kotlin.Deprecated

@Deprecated(
  message = "this is bad! bad kitty!",
  replaceWith = ReplaceWith("foo2()"),
  level = DeprecationLevel.ERROR
)
fun foo1() {}
fun foo2() {}

// will error and suggest replacing:
// foo1()