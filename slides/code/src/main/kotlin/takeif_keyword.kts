val input = "Kotlin"
val keyword = "in"

val index = input.indexOf(keyword)
    .takeIf { it >= 0 } ?: error("nope")
//  .takeUnless { it < 0 } ?: error("nope")

println("'$keyword' was found in '$input'")
println(input)
println(" ".repeat(index) + "^")
// 'in' was found in 'Kotlin'
// Kotlin
//     ^