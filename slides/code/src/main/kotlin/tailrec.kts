tailrec fun f1(n: Int, run: Int = 1): Int =
    if (n == 1) run
    else f1(n - 1, run * n)

tailrec fun f2(n: Int, a: Int, b: Int): Int =
    if (n == 0) a
    else f2(n - 1, b, a + b)