enum class DbState(
  override val sqlValue: String) : SqlEnum {
  A("a"), B("b");
  object Find : SqlFinder<DbState>(values())
}
interface SqlEnum { val sqlValue: String }
abstract class SqlFinder<E : SqlEnum>(
  values: Array<E>) {
  private val valuesBySqlKey =
    values.associateBy { it.sqlValue }
  fun orNull(searchValue: String): E? =
    valuesBySqlKey[searchValue]
}
DbState.Find.orNull("a")