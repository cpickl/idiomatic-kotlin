fun html(code: HtmlContext.() -> Unit) {
    code(HtmlContext())
}
@DslMarker annotation class HtmlDsl
@HtmlDsl class HtmlContext {
    fun head(code: HeadContext.() -> Unit) {
        code(HeadContext())
    }
}
@HtmlDsl class HeadContext { }