class Foo(map: Map<String, Any>) {
    val name: String by map
    val age: Int by map
}

Foo(mapOf(
    "name" to "bar",
    "age" to 42
))