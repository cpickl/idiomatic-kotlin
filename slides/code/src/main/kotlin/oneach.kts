listOf("foobar", "foo")
    .filter { it.endsWith("bar") }
    // chain item processing
    .onEach { println("Found item: $it") }
    .forEach { /* operate on them */ }