operator fun String.times(x: Int) =
  StringBuilder().apply {
  x.downTo(1).forEach {
    append(this@times)
  }
}.toString()

println("foo" * 3)