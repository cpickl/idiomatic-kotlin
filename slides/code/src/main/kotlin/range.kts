1.rangeTo(10)
// or in its operator form:
1..10

// "in" operator by ClosedRange interface
if(4 in 1..10) true

for(i in 1..10) true // t/m
for (i in 1 until 10) true // t/z
for (i in 1..10 step 2) true
for(i in 10 downTo 1) true
