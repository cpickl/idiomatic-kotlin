val list = listOf("anna", "otto")

/*Map<Int, String>*/ list.associateBy { it.length }
// == list.associate { it.length to it }

/*Map<String, Int>*/ list.associateWith { it.length }
// == list.associate { it to it.length }