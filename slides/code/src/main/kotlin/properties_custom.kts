class Foo {
  val bar: Int
    get() = baz + 42

  var baz = 0
    set (value) {
      require(value >= 0) {
        "invalid $value"
      }
      // backing field:
      field = value
      // baz = value!!!
  }
}