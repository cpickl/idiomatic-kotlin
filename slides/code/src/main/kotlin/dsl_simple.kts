fun html(code: HtmlContext.() -> Unit) {
    code(HtmlContext())
}
class HtmlContext {
    fun head(code: HeadContext.() -> Unit) {
        code(HeadContext())
    }
}
class HeadContext { }