/*

OOP principles:

1. abstraction (hiding internal implementation; like an Array/Linked-List)
2. encapsulation (attributes+methods united; data hiding + abstraction)
3. polymorphism (through subtyping; not inheritance/extending!)


*/
// =====================================================================================================================

interface Person {
    val prop1: Animal
    val prop2: Animal
    val prop3: Cat
    val prop4: Cat

    fun eat(animal: Animal)
    fun favoritePet(): Cat

    @kotlin.jvm.Throws(SubException::class)
    fun throwException()
}

class SubException(val something: String) : Exception()

interface Student : Person {
    override val prop1: Animal
    override val prop2: Cat
    override val prop3: Cat
    // contravariant property issue
    // override val prop4: Animal

    // covariant input parameter issue (caller using Person passes Animal, but requires Cat at runtime)
    fun eat(animal: Cat) { // doesn't override, but overloads in kotlin
        animal.meow()
    }

    // contravariant output parameter issue (caller might rely on Cat.meow)
    // override fun favoritePet(): Animal

    // contravariant exception issue (caller might rely on SubException.something
    @kotlin.jvm.Throws(Exception::class)
    override fun throwException()
}

interface Animal {
    fun nothing()
}

interface Cat : Animal {
    fun meow()
}

val animal: Animal = TODO()
val cat: Cat = TODO()
val person: Person = TODO()
val student: Student = TODO()

person.eat(animal)

// =====================================================================================================================


interface DataStructure<T> {
    fun read(): T
    fun write(t: T)
}

// type hierarchy: Any >> Number >> Int

val dataNumber: DataStructure<Number> = TODO()

// NOPE: val dataAny: DataParent<Any> = dataNumber
// NOPE: val dataAny: DataParent<in Any> = dataNumber
val dataAny: DataStructure<out Any> = dataNumber
// NOPE: dataAny.write("") ==> Nothing!
val yep = dataAny.read()

val dataInt: DataStructure<in Int> = dataNumber
val any: Any? = dataInt.read() // could be Int, or any other subtype of Number
dataInt.write(42) // any Int is also guaranteed to be a Number
