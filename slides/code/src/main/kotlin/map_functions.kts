var map = mapOf("x" to 1)
map.toMap() // create a copy
map.toMutableMap() // create a mutable copy

map += ("y" to 2)
map -= "y"

map.getValue("y") // throws
val map2 = map.withDefault { "!\$it!" }
map2.getValue("y") // !y!