import java.math.BigInteger

// already in SDK ;)
operator fun BigInteger.plus(x: BigInteger) =
  this.add(x)
BigInteger.ONE + BigInteger.TEN