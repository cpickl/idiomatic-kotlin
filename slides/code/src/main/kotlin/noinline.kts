inline fun partially(
  yes: () -> Unit,
  noinline no: () -> Unit
) {
  yes()
  no()
}

partially(
  { println("say yes") },
  { println("say no") }
)