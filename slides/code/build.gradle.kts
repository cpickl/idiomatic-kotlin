repositories {
    mavenCentral()
}

plugins {
    kotlin("jvm") version "1.7.10"
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-script-runtime:1.7.10")
}
