package com.cm.cpickl.idiomatickotlin

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.collections.shouldContainExactly
import org.jetbrains.exposed.sql.insert
import java.util.UUID

class RepositoryTest : StringSpec({
    val txId = UUID.randomUUID()
    val txAmount = 1337L

    "Given transaction exists When select all Then return it" {
        withTestDb {
            Transactions.insert {
                it[id] = txId
                it[amount] = txAmount
            }

            val dbos = ExposedTransactionRepository().selectAll()

            dbos shouldContainExactly listOf(TransactionDbo(id = txId, amount = txAmount))
        }
    }
})
