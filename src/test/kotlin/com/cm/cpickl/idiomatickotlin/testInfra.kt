package com.cm.cpickl.idiomatickotlin

import io.ktor.server.testing.ApplicationTestBuilder
import io.ktor.server.testing.testApplication
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction
import org.koin.dsl.ModuleDeclaration
import org.koin.dsl.module

fun ktorTest(testModule: ModuleDeclaration = {}, testCode: suspend ApplicationTestBuilder.() -> Unit) {
    testApplication {
        application {
            startupApplication(additonalModules = listOf(module(createdAtStart = true, testModule)))
        }
        testCode()
    }
}

fun withTestDb(code: () -> Unit) {
    val db =
        Database.connect("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1", driver = "org.h2.Driver", user = "root", password = "")
    transaction(db) {
        SchemaUtils.create(*allDatabaseTables)
        code()
        SchemaUtils.drop(*allDatabaseTables)
    }
}

fun Transaction.toJson() = """{ 
        "id": "${id.value}",
        "amount": $amount
    }"""
