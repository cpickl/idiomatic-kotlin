package com.cm.cpickl.idiomatickotlin

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import io.ktor.client.request.get
import io.ktor.client.request.header
import io.ktor.http.HttpStatusCode

class E2eTest : StringSpec({
    "When get transactions Then return ok" {
        ktorTest {
            val response = client.get("/transactions") {
                header("accept", "application/json")
            }

            response.status shouldBe HttpStatusCode.OK
        }
    }
})
