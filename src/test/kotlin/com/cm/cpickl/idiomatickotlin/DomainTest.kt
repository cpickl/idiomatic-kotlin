package com.cm.cpickl.idiomatickotlin

import io.kotest.assertions.arrow.core.shouldBeLeft
import io.kotest.assertions.arrow.core.shouldBeRight
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.core.spec.style.StringSpec
import io.kotest.core.test.TestCase
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.string.shouldContain
import io.mockk.every
import io.mockk.mockk

class TransactionTest : StringSpec({
    "When build transaction with negative amount Then throw" {
        val tx = Transaction(TransactionId.any(), amount = -1)

        tx.shouldBeLeft().message shouldContain "-1"
    }
})

class TransactionServiceImplTest : DescribeSpec() {

    private val txDbo = TransactionDbo.any()
    private var repo = mockk<TransactionRepository>()

    override suspend fun beforeEach(testCase: TestCase) {
        repo = mockk()
    }

    init {
        describe("getAllTransactions") {
            it("Given no transactions Then return empty") {
                every { repo.selectAll() } returns emptyList()

                val result = TransactionServiceImpl(repo).getAllTransactions()

                result shouldBeRight emptyList()
            }
            it("Given transaction Then return transformed version") {
                every { repo.selectAll() } returns listOf(txDbo)

                val result = TransactionServiceImpl(repo).getAllTransactions()

                (result.shouldBeRight()) shouldContainExactly listOf(
                    Transaction(
                        id = TransactionId(txDbo.id),
                        amount = txDbo.amount
                    ).shouldBeRight()
                )
            }
        }
    }
}
