package com.cm.cpickl.idiomatickotlin

import io.kotest.assertions.arrow.core.shouldBeRight
import io.kotest.property.Arb
import io.kotest.property.arbitrary.arbitrary
import io.kotest.property.arbitrary.long
import io.kotest.property.arbitrary.next
import io.kotest.property.arbitrary.uuid

fun Transaction.Companion.arb() = arbitrary {
    Transaction(
        id = TransactionId.any(),
        amount = Arb.long(min = 0).bind()
    ).shouldBeRight()
}

fun Transaction.Companion.any() = arb().next()

fun TransactionId.Companion.arb() = arbitrary {
    TransactionId(
        Arb.uuid().bind()
    )
}

fun TransactionId.Companion.any() = arb().next()

fun TransactionDbo.Companion.arb() = arbitrary {
    TransactionDbo(
        id = Arb.uuid().bind(),
        amount = Arb.long(min = 0).bind()
    )
}

fun TransactionDbo.Companion.any() = arb().next()
