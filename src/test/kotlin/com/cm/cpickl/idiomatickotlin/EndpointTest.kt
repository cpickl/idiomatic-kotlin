package com.cm.cpickl.idiomatickotlin

import arrow.core.right
import io.kotest.assertions.json.shouldEqualJson
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import io.ktor.client.request.get
import io.ktor.client.request.header
import io.ktor.client.statement.bodyAsText
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.http.contentType
import io.mockk.coEvery
import io.mockk.mockk
import org.koin.dsl.bind

class EndpointTest : StringSpec({
    "When get transactions Then return transactions" {
        val tx = Transaction.any()
        ktorTest(testModule = {
            val service = mockk<TransactionService>()
            coEvery { service.getAllTransactions() } returns listOf(tx).right()
            single { service } bind TransactionService::class
        }) {

            val response = client.get("/transactions") {
                header("accept", "application/json")
            }

            response.status shouldBe HttpStatusCode.OK
            response.contentType()?.withoutParameters() shouldBe ContentType.Application.Json
            response.bodyAsText() shouldEqualJson """{ "transactions": [ ${tx.toJson()} ] }"""
        }
    }
})
