package com.cm.cpickl.idiomatickotlin

import mu.KotlinLogging.logger
import org.jetbrains.exposed.dao.id.UUIDTable
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import java.util.UUID

private val log = logger {}

fun connectToDatabase() {
    log.info { "Connecting to local in-memory database..." }
    Database.connect("jdbc:h2:mem:prod;DB_CLOSE_DELAY=-1", driver = "org.h2.Driver", user = "root", password = "")
    transaction {
        log.debug { "Creating schema for tables: ${allDatabaseTables.joinToString()}" }
        SchemaUtils.create(*allDatabaseTables)
    }
}

interface TransactionRepository {
    fun selectAll(): List<TransactionDbo>
}

class ExposedTransactionRepository : TransactionRepository {
    override fun selectAll(): List<TransactionDbo> =
        transaction {
            Transactions.selectAll().toList().map {
                TransactionDbo.from(it)
            }
        }
}

data class TransactionDbo(
    val id: UUID,
    val amount: Long
) {
    companion object {
        fun from(row: ResultRow) = TransactionDbo(
            id = row[Transactions.id].value,
            amount = row[Transactions.amount]
        )
    }
}

object Transactions : UUIDTable("transactions", columnName = "id") {
    val amount = long("amount")
}

val allDatabaseTables = arrayOf(Transactions)
