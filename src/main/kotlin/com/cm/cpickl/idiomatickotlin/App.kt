package com.cm.cpickl.idiomatickotlin

import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import mu.KotlinLogging.logger
import org.koin.dsl.bind
import org.koin.dsl.module

object App {

    private val log = logger {}

    @JvmStatic
    fun main(args: Array<String>) {
        log.info { "Starting up server on port 8080..." }
        embeddedServer(Netty, port = 8080) {
            startupApplication()
        }.start(wait = true)
    }
}

val koinModule = module {
    single { ExposedTransactionRepository() } bind TransactionRepository::class
    single { TransactionServiceImpl(get()) } bind TransactionService::class
}
