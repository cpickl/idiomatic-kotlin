package com.cm.cpickl.idiomatickotlin

import arrow.core.right
import io.ktor.server.application.Application
import io.ktor.server.routing.get
import io.ktor.server.routing.routing
import kotlinx.serialization.Serializable
import mu.KotlinLogging.logger
import org.koin.ktor.ext.inject

private val log = logger {}

fun Application.installRouting() {
    val transactionService by inject<TransactionService>()
    routing {
        get("/transactions") {
            eitherCall {
                log.info { "GET /transactions" }
                val allTransactions = transactionService.getAllTransactions().bind()
                TransactionsDto(allTransactions.map { it.toDto() }).right()
            }
        }
    }
}

private fun Transaction.toDto() = TransactionDto(
    id = id.value.toString(),
    amount = amount
)

@Serializable
data class TransactionsDto(
    val transactions: List<TransactionDto>
) : Dto

@Serializable
data class TransactionDto(
    val id: String,
    val amount: Long
) : Dto
