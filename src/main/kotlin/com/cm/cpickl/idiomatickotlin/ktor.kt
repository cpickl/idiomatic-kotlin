package com.cm.cpickl.idiomatickotlin

import arrow.core.Either
import arrow.core.continuations.EffectScope
import arrow.core.continuations.either
import io.ktor.http.HttpStatusCode
import io.ktor.serialization.kotlinx.json.json
import io.ktor.server.application.Application
import io.ktor.server.application.ApplicationCall
import io.ktor.server.application.call
import io.ktor.server.application.install
import io.ktor.server.plugins.contentnegotiation.ContentNegotiation
import io.ktor.server.response.respond
import io.ktor.util.pipeline.PipelineContext
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import org.koin.core.module.Module
import org.koin.ktor.plugin.Koin

fun Application.startupApplication(additonalModules: List<Module> = emptyList()) {
    connectToDatabase()
    install(Koin) {
        modules(mutableListOf(koinModule).plus(additonalModules))
    }
    install(ContentNegotiation) {
        json(Json {
            prettyPrint = true
        })
    }
    installRouting()
}

suspend inline fun <DTO : Dto> PipelineContext<Unit, ApplicationCall>.eitherCall(crossinline function: suspend EffectScope<Evil>.() -> Either<Evil, DTO>) {
    val apiResponse = either {
        function().bind()
    }.fold(
        ifLeft = {
            when (it) {
                is InvalidArgumentEvil -> ApiResponse(HttpStatusCode.InternalServerError, EvilDto("nope"))
            }
        },
        ifRight = {
            ApiResponse(HttpStatusCode.OK, it)
        }
    )
    call.respond(apiResponse.status, apiResponse.dto)
}

/** Marker interface. */
interface Dto

@Serializable
data class EvilDto(
    val message: String
) : Dto

data class ApiResponse(
    val status: HttpStatusCode,
    val dto: Dto
)
