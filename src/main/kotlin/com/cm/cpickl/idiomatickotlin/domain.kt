package com.cm.cpickl.idiomatickotlin

import arrow.core.Either
import arrow.core.continuations.either
import arrow.core.left
import arrow.core.right
import java.util.UUID

interface TransactionService {
    suspend fun getAllTransactions(): Either<Evil, List<Transaction>>
}

data class Transaction private constructor(
    val id: TransactionId,
    val amount: Long
) {
    companion object {
        operator fun invoke(
            id: TransactionId,
            amount: Long
        ): Either<Evil, Transaction> =
            if (amount < 0) {
                InvalidArgumentEvil("Transaction.amount must be >= 0, but was: $amount").left()
            } else {
                Transaction(id, amount).right()
            }
    }
}

@JvmInline
value class TransactionId(val value: UUID) {
    companion object;
}

class TransactionServiceImpl(
    private val repository: TransactionRepository
) : TransactionService {
    override suspend fun getAllTransactions() = either {
        repository.selectAll().map {
            Transaction(
                id = TransactionId(it.id),
                amount = it.amount
            ).bind()
        }
    }
}

sealed class Evil(
    val message: String,
    val cause: Throwable? = null
)

class InvalidArgumentEvil(
    message: String,
    cause: Throwable? = null
) : Evil(message, cause)
