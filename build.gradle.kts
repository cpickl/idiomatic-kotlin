import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

repositories {
    mavenCentral()
}

plugins {
    kotlin("jvm") version "1.7.10"
    kotlin("plugin.serialization") version "1.7.10"
}

dependencies {
    implementation("io.ktor:ktor-server-core:2.0.3")
    implementation("io.ktor:ktor-server-netty:2.0.3")
    implementation("io.ktor:ktor-server-content-negotiation:2.0.3")
    implementation("io.ktor:ktor-serialization-kotlinx-json:2.0.3")
    implementation("io.insert-koin:koin-ktor:3.2.0")
    implementation("org.jetbrains.exposed:exposed-core:0.39.1")
    implementation("org.jetbrains.exposed:exposed-dao:0.39.1")
    implementation("org.jetbrains.exposed:exposed-jdbc:0.39.1")
    runtimeOnly("com.h2database:h2:2.1.214") // used for prod & test
    implementation("io.arrow-kt:arrow-core:1.1.2")
    implementation("io.github.microutils:kotlin-logging-jvm:2.1.23")
    runtimeOnly("ch.qos.logback:logback-classic:1.2.11")

    testImplementation("io.ktor:ktor-server-test-host:2.0.3") {
        exclude(group = "junit", module = "junit")
        exclude(group = "org.jetbrains.kotlin", module = "kotlin-test-junit")
    }
    testImplementation("io.kotest:kotest-runner-junit5:5.4.1") {
        exclude(group = "org.junit.jupiter", module = "junit-jupiter-api")
    }
    testImplementation("io.kotest:kotest-assertions-core-jvm:5.4.1")
    testImplementation("io.kotest:kotest-assertions-json:5.4.1")
    testImplementation("io.kotest.extensions:kotest-assertions-arrow:1.2.5")
    testImplementation("io.kotest:kotest-property:5.4.1")
    testImplementation("io.mockk:mockk:1.12.5")

}

tasks.withType<KotlinCompile>().configureEach {
    kotlinOptions {
        freeCompilerArgs = listOf(
            "-Xjsr305=strict"
        )
        jvmTarget = "11"
    }
}

tasks.withType<Test>().configureEach {
    useJUnitPlatform()
}
