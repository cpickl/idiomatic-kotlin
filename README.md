# idiomatic-kotlin

container project to hold code shown at presentations at the CM.com/payplaza devmeeting by christoph pickl.

* "idiomatic kotlin - tools build with kotlin for kotlin", 4th august
  2022 ([video recording](https://payplasa-my.sharepoint.com/:v:/r/personal/agh_payplaza_com/Documents/Opnamen/developer%20meeting-20220804_110148-Meeting%20Recording.mp4?csf=1&web=1&e=q7FtQa), [sourcecode](https://gitlab.com/cpickl/idiomatic-kotlin/-/commits/step-by-step/src/main/kotlin/com/cm/cpickl/idiomatickotlin), [notes](https://gitlab.com/cpickl/idiomatic-kotlin/-/raw/main/slides/tools/step_guide.pdf))
* "idiomatic kotlin - let's talk about code baby", 18th august 2022 (video recording [part 1](https://payplasa-my.sharepoint.com/:v:/g/personal/cpickl_payplaza_com/EfvyGt4e0odLtyAGvt5i-toBcqiGeh6jKBN96JuCpUzReA) and [part 2](https://payplasa-my.sharepoint.com/:v:/g/personal/cpickl_payplaza_com/EVg7eLA2rB1Ci5xsDIhqPIcBVUamZ1Y_asJxKyd5K7Ljsg), [slides](https://gitlab.com/cpickl/idiomatic-kotlin/-/raw/main/slides/code/idiomatic_kotlin_code_slides-2022_08_18.pdf), [sourcecode](https://gitlab.com/cpickl/idiomatic-kotlin/-/tree/main/slides/code/src/main/kotlin))

## tech-stack

* [ktor](https://ktor.io/) ... web framework (= spring/quarkus)
* [exposed](https://github.com/JetBrains/Exposed) ... persistence (= hibernate)
* [koin](https://insert-koin.io) ... dependency injection (= spring/guice)
* [kotlinx.serialization](https://github.com/Kotlin/kotlinx.serialization) ... JSON serialization (= jackson/gson)
* [arrow](https://arrow-kt.io/) ... functional kotlin (= functionaljava)
* [mu-logging](https://github.com/MicroUtils/kotlin-logging) ... logging (= slf4j)
* [kotest](https://kotest.io/) ... testing (= junit/testng)
  + [IntelliJ plugin](https://kotest.io/docs/intellij/intellij-plugin.html)
* [mockk](https://mockk.io/) ... mocking (= mockito/easymock)

## ideas for future presentations

* ~~kotlin libs~~
* ~~kotlin code~~
* kotlin advanced (coroutines, kts, context receivers, reflection)
* testing
* functional fundamentals
* better java integration
* multiplatform
* timesheet app with harvest integration
* tornadoFX